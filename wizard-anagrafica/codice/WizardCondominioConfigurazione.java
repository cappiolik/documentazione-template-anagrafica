package eu.enumera.caloria.commons.anagrafica;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import edu.emory.mathcs.backport.java.util.Arrays;
import eu.enumera.caloria.caloriamanutenzione.viewmodels.lookup.FamigliaStrumenti;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.lang.Nullable;

import java.sql.Date;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.*;//Blank;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY, getterVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY)
public class WizardCondominioConfigurazione {

    // static (at least until there is only one)
    private static  List<String> knownDefinitions = Arrays.asList(new String[]
            { "CodiceCondominio" , "CodiceGestore" , "NomeCondominio" , "IndirizzoCondominio" , "CittaCondominio" , "ProvinciaCondominio" ,"LinkPLC",
                    "SeparatoreCSV", "SeparatoriElementiLista" ,
                    "DataPrimaLetturaConvenzionale" , "Stagione" , "Amministratore" , "Tecnico" ,
                    "DefinizioneParziale" ,  "PolicyEliminaMatricoleMancanti", "PolicyEliminaAssociazioniMancanti" ,
                    "TipoStrumentoDefault", "TipoStrumentoCondominialeDefault" , "ProduttoreDefault" ,
                    "UtenzeConvenzionali" , "StrumentoCondominialeSeUtenzaVuota" , "StrumentoCondominialeSeUtenzaConvenzionale",
                    "ColonnaMatricola" , "ColonnaNoteStrumento" , "ColonnaTipoStrumento" , "ColonnaFattoreRivalutazione" , "ColonnaProduttore" , "ColonnaVano",
                    "ColonnaUtenza"  /*, "ColonnaRiferimentoSistemaUtenza"*/ , "ColonnaUtenzaNote" , "ColonnaUtenzaScala" , "ColonnaUtenzaSub", "ColonnaUtenzaInterno", "ColonnaUtenzaPiano" , "ColonnaUtenzaContatto" ,"ColonnaUtenzaRifExt"
            });

    private static List<String> workingDefinitions = knownDefinitions.stream().map(kd -> kd.toLowerCase()).collect(Collectors.toList());
    public List<Definition> processedDefinitions = new ArrayList<>();

    public static WizardCondominioConfigurazione defaultInstance() {
        WizardCondominioConfigurazione c = new WizardCondominioConfigurazione();
        return c;
    }



    public boolean isKnownDefinition(String def) {
        if (workingDefinitions.contains(StringUtils.trimToEmpty(def).toLowerCase()))
            return true;
        return false;
    }

    /**
     * @param definition (case insentive)
     * @param value
     * @return true if definition is known
     */
    public boolean define(String definition, String value) {
        if (! isKnownDefinition(definition)) {
            return false;
        }
        processedDefinitions.add(Definition.of(definition, value));
        definition = StringUtils.trimToEmpty(definition).toLowerCase();

        // coindominio, gestore e generale
        if ("codicecondominio".equals(definition))
            codiceCondominio = removeCSVSepCharsInValueToken(StringUtils.trimToNull(value));
        if ("nomecondominio".equals(definition))
            nomeCondominio = removeCSVSepCharsInValueToken(StringUtils.trimToEmpty(value));
        if ("cittacondominio".equals(definition))
            cittaCondominio = removeCSVSepCharsInValueToken(StringUtils.trimToEmpty(value));
        if ("provinciacondominio".equals(definition))
            provinciaCondominio = removeCSVSepCharsInValueToken(StringUtils.trimToEmpty(value));
        if ("indirizzocondominio".equals(definition))
            indirizzoCondominio = removeCSVSepCharsInValueToken(StringUtils.trimToNull(value));
        if ("codicegestore".equals(definition))
            codiceGestore = removeCSVSepCharsInValueToken(StringUtils.trimToNull(value));
        if ("linkplc".equals(definition))
            linkPLC = removeCSVSepCharsInValueToken(StringUtils.trimToNull(value));
        if ("dataprimaletturaconvenzionale".equals(definition))
            dataPrimaLetturaConvenzionale = removeCSVSepCharsInValueToken(StringUtils.trimToNull(value));

        if ("stagione".equals(definition))
             addStagione(removeCSVSepCharsInValueToken(value));
        if ("amministratore".equals(definition))
             addAmministratore(removeCSVSepCharsInValueToken(value));
        if ("tecnico".equals(definition))
             addTecnico(removeCSVSepCharsInValueToken(value));


        // sintassi e meta template
        if ("separatorecsv".equals(definition)) {
            if (StringUtils.isNotBlank(value)) {
                value = StringUtils.trimToEmpty(value).toLowerCase();
                if (value.indexOf(";") == 0) separatoreCSV = ";";
                else if (value.contains("puntoevirgola")) separatoreCSV = ";";
                else if (value.contains("puntovirgola")) separatoreCSV = ";";
                else if (value.contains("space")) separatoreCSV = " ";
                else if (value.contains("spazio")) separatoreCSV = " ";
                else if (value.contains("tab")) separatoreCSV = "\t";
                else if (value.contains("virgola")) separatoreCSV = ",";
                else if (value.contains(",")) separatoreCSV = ",";
            }
        }
        if ("separatorielementilista".equals(definition) && !isBlank(value))
            separatoriElementiLista = removeCSVSepCharsInValueToken(StringUtils.trimToEmpty(value))
                    .replace("puntoevirgola" , ";").replace("puntovirgola", ";").replace("space" , " ").replace("tab" , "\t").replace("spazio" , " ")
                    .replace("virgola" , ",");

        //policies
        if ("definizioneparziale".equals(definition)) {
            try {
                definizioneParziale = (Boolean.valueOf(removeCSVSepCharsInValueToken(StringUtils.trimToEmpty(value))));
            } catch (Exception e) {e.printStackTrace();}
        }
        if ("policyeliminamatricolemancanti".equals(definition)) {
            try {
                policyEliminaMatricoleMancanti = Boolean.valueOf(removeCSVSepCharsInValueToken(StringUtils.trimToEmpty(value)));
            } catch (Exception e) {e.printStackTrace();}
        }
        if ("policyeliminaassociazionimancanti".equals(definition)) {
            try {
                policyEliminaAssociazioniMancanti = Boolean.valueOf(removeCSVSepCharsInValueToken(StringUtils.trimToEmpty(value)));
            } catch (Exception e) {e.printStackTrace();}
        }


        //strumenti condominiali
        if ("utenzeconvenzionali".equals(definition))
           setUtenzeConvenzionali(removeCSVSepCharsInValueToken(StringUtils.trimToEmpty(value)));
        if ("strumentocondominialeseutenzaconvenzionale".equals(definition)) {
            try {
                strumentoCondominialeSeUtenzaConvenzionale = Boolean.valueOf(removeCSVSepCharsInValueToken(StringUtils.trimToEmpty(value)));
            } catch (Exception e) {e.printStackTrace();}
        }
        if ("strumentocondominialeseutenzavuota".equals(definition)) {
            try {
                strumentoCondominialeSeUtenzaVuota = Boolean.valueOf(removeCSVSepCharsInValueToken(StringUtils.trimToEmpty(value)));
            } catch (Exception e) {e.printStackTrace();}
        }
        if ("tipostrumentocondominialedefault".equals(definition))
            tipoStrumentoCondominialeDefault = removeCSVSepCharsInValueToken(StringUtils.trimToNull(value));


        //strumento
        if ("colonnamatricola".equals(definition))
            colonnaMatricola = removeCSVSepCharsInValueToken(StringUtils.trimToNull(value));
        if ("tipostrumentodefault".equals(definition))
            tipoStrumentoDefault = removeCSVSepCharsInValueToken(StringUtils.trimToNull(value));
        if ("produttoredefault".equals(definition))
            produttoreDefault = removeCSVSepCharsInValueToken(StringUtils.trimToNull(value));
        if ("colonnavano".equals(definition)) {
            colonnaVano =removeCSVSepCharsInValueToken( StringUtils.trimToNull(value));
        }
        if ("colonnanotestrumento".equals(definition)) {
            colonnaNoteStrumento =  removeCSVSepCharsInValueToken(StringUtils.trimToNull(value));
        }
        if ("colonnatipostrumento".equals(definition)) {
            colonnaTipoStrumento = removeCSVSepCharsInValueToken(StringUtils.trimToNull(value));
        }
        if ("colonnaproduttore".equals(definition)) {
            colonnaProduttore =removeCSVSepCharsInValueToken( StringUtils.trimToNull(value));
        }
        if ("colonnafattorerivalutazione".equals(definition)) {
            colonnaFattoreRivalutazione = removeCSVSepCharsInValueToken(StringUtils.trimToNull(value).replace(",", "."));
        }
        //utenza
        if ("colonnautenza".equals(definition)) {
            colonnaUtenza = removeCSVSepCharsInValueToken(StringUtils.trimToNull(value));
        }
        if ("colonnautenzacontatto".equals(definition)) {
            colonnaUtenzaContatto = removeCSVSepCharsInValueToken(StringUtils.trimToNull(value));
        }
        if ("colonnautenzanote".equals(definition)) {
            colonnaUtenzaNote = removeCSVSepCharsInValueToken(StringUtils.trimToNull(value));
        }
        if ("colonnautenzapiano".equals(definition)) {
            colonnaUtenzaPiano = removeCSVSepCharsInValueToken(StringUtils.trimToNull(value));
        }
        if ("colonnautenzascala".equals(definition)) {
            colonnaUtenzaScala = removeCSVSepCharsInValueToken(StringUtils.trimToNull(value));
        }
        if ("colonnautenzainterno".equals(definition)) {
            colonnaUtenzaInterno = removeCSVSepCharsInValueToken(StringUtils.trimToNull(value));
        }
        if ("colonnautenzasub".equals(definition)) {
            colonnaUtenzaSub = removeCSVSepCharsInValueToken(StringUtils.trimToNull(value));
        }
        if ("colonnautenzarifext".equals(definition)) {
            colonnaUtenzaRifExt = removeCSVSepCharsInValueToken(StringUtils.trimToNull(value));
        }

//        if ("colonnariferimentosistemautenza".equals(definition)) {
//            colonnaRiferimentoSistemaUtenza = StringUtils.trimToNull(value);
//        }



        return true;
    }

    //check if useful
    public boolean isCondominioRicercabile() {
        return (codiceCondominio != null && codiceGestore != null);
    }

    public static @Nullable Pair<String,String> extractDefinitionAndValue(String line) {
       if (isBlank(line))
           return null;
       line = StringUtils.trimToEmpty(line);
       if (! line.startsWith("##")) return null;
//       if (line.startsWith("##")) return null;
       line = line.replaceFirst("##", "");
       String[] tokens = StringUtils.split(line, '=');
       if (tokens.length <2)
            return null;
       return Pair.of(tokens[0] , tokens[1]);
    }

    //condominio e generali
    private String codiceGestore;
    private String codiceCondominio;
    private String nomeCondominio;
    private String cittaCondominio;
    private String provinciaCondominio;
    private String indirizzoCondominio;
    private String separatoreCSV = ";";
    private String separatoriElementiLista = "|";
    private String dataPrimaLetturaConvenzionale;
    private List<Pair<String, Integer>> stagioni = new ArrayList<>();
    private String linkPLC;
    private List<String> amministratori = new ArrayList<>(3);
    private List<String> tecnici = new ArrayList<>(3);


    //utenze
    private String colonnaUtenza;
    private String colonnaUtenzaContatto;
    private String colonnaUtenzaNote;
    private String colonnaUtenzaInterno;
    private String colonnaUtenzaSub;
    private String colonnaUtenzaPiano;
    private String colonnaUtenzaScala;
    private String colonnaUtenzaRifExt;
    //private String colonnaRiferimentoSistemaUtenza;

    //strumenti
    private String tipoStrumentoDefault = "HCA";
    private String tipoStrumentoCondominialeDefault = "Condominiale calore";
    private String produttoreDefault;
    private String colonnaMatricola;
    private String colonnaVano;
    private String colonnaTipoStrumento;
    private String colonnaNoteStrumento;
    private String colonnaProduttore;
    private String colonnaFattoreRivalutazione;

    //policy meta
    private Boolean policyEliminaMatricoleMancanti = false;
    private Boolean policyEliminaAssociazioniMancanti = false;
    private Boolean definizioneParziale = false;
    private Boolean strumentoCondominialeSeUtenzaVuota = true;
    private Boolean strumentoCondominialeSeUtenzaConvenzionale = true;
    private List<String> utenzeConvenzionali = new ArrayList<>(2);

    //GETTERS SETTERS
    //not using optional
    public @Nullable String getCodiceGestore() {return codiceGestore;}
    public @Nullable String getCodiceCondominio() {return codiceCondominio;}
    public @Nullable String getNomeCondominio() {return nomeCondominio;}
    public @Nullable String getCittaCondominio() {return cittaCondominio;}
    public @Nullable String getLinkPLC() {return linkPLC;}
    public @Nullable String getProvinciaCondominio() {return provinciaCondominio;}
    public @Nullable String getIndirizzoCondominio() {return indirizzoCondominio;}
    public @Nullable String getSeparatoreCSV() {return separatoreCSV;}
    public @Nullable String getSeparatoriElementiLista() {return separatoriElementiLista;}
    public @Nullable String getColonnaMatricola() {return colonnaMatricola;}
    public @Nullable String getColonnaFattoreRivalutazione() {return colonnaFattoreRivalutazione;}
    public @Nullable String getColonnaVano() {return colonnaVano;}
    public @Nullable String getColonnaTipoStrumento() {return colonnaTipoStrumento;}
    public @Nullable String getColonnaProduttore() {return colonnaProduttore;}
    public @Nullable String getColonnaNoteStrumento() {return colonnaNoteStrumento;}
    public @Nullable String getTipoStrumentoDefault() {return tipoStrumentoDefault;}
    public @Nullable String getTipoStrumentoCondominialeDefault() {return tipoStrumentoCondominialeDefault;}
    //utenza
    public @Nullable String getColonnaUtenza() {return colonnaUtenza;}
    public @Nullable String getColonnaUtenzaNote() {return colonnaUtenzaNote;}
    public @Nullable String getColonnaUtenzaPiano() {return colonnaUtenzaPiano;}
    public @Nullable String getColonnaUtenzaContatto() {return colonnaUtenzaContatto;}
    public @Nullable String getColonnaUtenzaScala() {return colonnaUtenzaScala;}
    public @Nullable String getColonnaUtenzaInterno() {return colonnaUtenzaInterno;}
    public @Nullable String getColonnaUtenzaSub() {return colonnaUtenzaSub;}
    public @Nullable String getColonnaUtenzaRifExt() {return colonnaUtenzaRifExt;}
    //public @Nullable String getColonnaRiferimentoSistemaUtenza() {return colonnaRiferimentoSistemaUtenza;}
    //policy meta
    public @Nullable Boolean getPolicyEliminaMatricoleMancanti() {return policyEliminaMatricoleMancanti;}
    public @Nullable Boolean getPolicyEliminaAssociazioniMancanti() {return policyEliminaAssociazioniMancanti;}
    public @Nullable Boolean getDefinizioneParziale() {return definizioneParziale;}
    public @Nullable Boolean getStrumentoCondominialeSeUtenzaVuota() {return strumentoCondominialeSeUtenzaVuota;}
    public @Nullable Boolean getStrumentoCondominialeSeUtenzaConvenzionale() {return strumentoCondominialeSeUtenzaConvenzionale;}
    public @Nullable List<String> getUtenzeConvenzionali() {return utenzeConvenzionali;}
    public @Nullable List<String> getAmministratori() {return amministratori;}
    public @Nullable List<String> getTecnici() {return tecnici;}
    public @Nullable String getProduttoreDefault() {return produttoreDefault;}
    public List<Pair<String, Integer>> getStagioni() {return stagioni;}
    public Optional<LocalDate> getDataPrimaLetturaConvenzionale() {
        try {
            return Optional.of(Date.valueOf(dataPrimaLetturaConvenzionale).toLocalDate());
        } catch (Exception e) {
            return Optional.empty();
        }
    }


    public void setUtenzeConvenzionali(String singleValueOrListWithDefinedSeparator) {
        if (StringUtils.isNotBlank(singleValueOrListWithDefinedSeparator)) {
            String[] tokens = StringUtils.splitByWholeSeparator(singleValueOrListWithDefinedSeparator, getSeparatoriElementiLista());
            utenzeConvenzionali.clear();
            for (String token: tokens
                 ) {
                if (StringUtils.isNotBlank(token)) {
                    utenzeConvenzionali.add(StringUtils.trimToEmpty(token));
                }
            }
        }
    }

    protected void addStagione(String rawValueStagione) {
        String[] tokens = StringUtils.split(StringUtils.trimToEmpty(rawValueStagione), ";|,");
        if (tokens.length == 2) {
            stagioni.add(Pair.of(tokens[0], FamigliaStrumenti.resolveIdOrDefault(tokens[1])));
        } else if (tokens.length ==1) {
            stagioni.add(Pair.of(tokens[0], FamigliaStrumenti.defaultId));
        }
    }

    protected void addAmministratore(String amm) {
        if (isBlank(amm)) return;
        amm = trim(amm);
        if (! amministratori.contains(amm))
            amministratori.add(amm);
    }

    protected void addTecnico(String tecnico) {
        if (isBlank(tecnico)) return;
        tecnico = trim(tecnico);
        if (! tecnici.contains(tecnico))
            tecnici.add(tecnico);
    }

    private String removeCSVSepCharsInValueToken(String valueToken) {
        if (valueToken == null)
            return null;
        return valueToken.replace(getSeparatoreCSV(), "");
    }

    public static class Definition  {
        public String def;
        public Object val;
        public static Definition of (String definition, Object value) {
            Definition d = new Definition(); d.def = definition; d.val = value; return d;
        }
    }

    /**
     * Pensata per l'utilizzo in reverse templating (da dati a template)
     * non possibile per ora usare le definizioni per costruire dinamicamente le posizioni
     * @return
     */
    @Deprecated
    public static WizardCondominioConfigurazione referenceConfiguration() {
        WizardCondominioConfigurazione c = new WizardCondominioConfigurazione();
        c.colonnaMatricola = "0";
        c.colonnaUtenza = "1";
        c.colonnaVano = "2";
        c.colonnaTipoStrumento = "3";
        c.colonnaFattoreRivalutazione = "4";
        c.colonnaNoteStrumento = "5";
        c.colonnaProduttore = "6";
        c.codiceGestore = "ENUMERA";

        return c;
    }




}
