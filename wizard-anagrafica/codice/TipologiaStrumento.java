package eu.enumera.caloria.commons.definitions.strumenti;

import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum TipologiaStrumento {

    Sconosciuto(0, "Sconosciuto" , "unknown" , "n/a" , "N/d"),
    Ripartitore(1, "Ripartitore" , "rip" , "hca" ,"ripartitore" , "ripartitore calore" , "ripartitore di calore"),
    ContatoreDiretto(9, "Contatore diretto" , "ccd" ),
    AcquaCalda(16, "Acqua calda" , "hot" , "hw" , "acs"),
    AcquaFredda(32, "Acqua fredda" , "cold" , "cw", "afs" ),
    CondominialeCalore(100, "Condominiale Calore" , "ctccd", "condominio", "centrale termica" , "condominiale" ),
    @Deprecated  Centrale(101, "Centrale"),
    ;

    private final Integer id;
    private final String nome;
    private final List<String> alias;

    private TipologiaStrumento(Integer id, String nome, String... alias) {
        this.id = id;
        this.nome = nome;
        this.alias = Arrays.asList(alias).stream().map(t -> StringUtils.trimToNull(t)).filter(t -> t!=null).map(t -> ((String) t).toLowerCase()).collect(Collectors.toList());
    }

    public String getNome() {return nome;}
    public Integer getId() {return id;}

    public static @NonNull
    TipologiaStrumento resolve(String tipo) {
        for (TipologiaStrumento t: values()) {
            tipo = StringUtils.trimToEmpty(tipo).toLowerCase();
            if (t.getNome().toLowerCase().equalsIgnoreCase(tipo))
                return t;
            if (t.alias.contains(tipo)) return t;
        }
        return TipologiaStrumento.Sconosciuto;
    }
}
